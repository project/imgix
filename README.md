Imgix
======================

> Render Drupal 8 images through Imgix, a real-time image processing service and CDN

## Why?
- **Integrate the Imgix service with image styles**. You can keep your existing image styles, just switch the toolkit.
- **Offload thumbnail generation** to an external service. Especially handy when trying to render pages with a lot of
  images, like the admin media gallery.
- **Use advanced image manipulation** like auto-centering resized images on faces, cropping on a focal point, adding 
  text, adding watermarks, etc.

## Installation

This package requires PHP 7.4 and Drupal 8.8.4 or higher. It can be
installed using Composer:

```bash
 composer require drupal/imgix
```

## Changelog
All notable changes to this project will be documented in the
[CHANGELOG](CHANGELOG.md) file.
