<?php

namespace Drupal\imgix\Plugin\ImageToolkit;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\ImageToolkit\ImageToolkitBase;
use Drupal\Core\ImageToolkit\ImageToolkitInterface;
use Drupal\Core\Render\Element;
use Drupal\file_mdm\FileMetadataManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * An image toolkit for Imgix, a real-time image processing service and CDN.
 *
 * @ImageToolkit(
 *   id = "imgix",
 *   title = @Translation("Imgix toolkit")
 * )
 */
class ImgixToolkit extends ImageToolkitBase implements ImgixToolkitInterface {

  /**
   * A static cache for the width of the image.
   *
   * @var int
   */
  protected $width;

  /**
   * A static cache for the height of the image.
   *
   * @var int
   */
  protected $height;

  /**
   * The image toolkit manager.
   *
   * @var \Drupal\Core\ImageToolkit\ImageToolkitManager
   */
  protected $imageToolkitManager;

  /**
   * The params that will be sent to Imgix along with the image.
   *
   * @var array
   */
  protected $params = [];

  /**
   * The fallback image toolkit.
   *
   * @var \Drupal\Core\ImageToolkit\ImageToolkitInterface
   */
  protected $fallbackToolkit;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('image.toolkit.operation.manager'),
      $container->get('logger.channel.image'),
      $container->get('config.factory')
    );
    $instance->imageToolkitManager = $container->get('image.toolkit.manager');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function isValid(): bool {
    if ($this->useFallbackToolkit()) {
      return $this->getFallbackToolkit()->isValid();
    }

    // Let's assume the image is valid, this toolkit doesn't care.
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function save($destination) {
    if ($this->useFallbackToolkit($destination)) {
      return $this->getFallbackToolkit()->save($destination);
    }

    // No changes to the actual files are necessary to use this toolkit.
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function parseFile(): bool {
    if ($this->useFallbackToolkit()) {
      return $this->getFallbackToolkit()->parseFile();
    }

    // Let's assume the image is valid, this toolkit doesn't care.
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getWidth(): ?int {
    if ($this->useFallbackToolkit()) {
      return $this->getFallbackToolkit()->getWidth();
    }

    if (isset($this->width)) {
      return $this->width;
    }

    $this->loadInfo();

    return $this->width;
  }

  /**
   * {@inheritdoc}
   */
  public function getHeight(): ?int {
    if ($this->useFallbackToolkit()) {
      return $this->getFallbackToolkit()->getHeight();
    }

    if (isset($this->height)) {
      return $this->height;
    }

    $this->loadInfo();

    return $this->height;
  }

  /**
   * {@inheritdoc}
   */
  public function getMimeType(): ?string {
    if ($this->useFallbackToolkit()) {
      return $this->getFallbackToolkit()->getMimeType();
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getParameter(string $key) {
    return $this->params[$key] ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setParameter(string $key, string $value) {
    $this->params[$key] = $value;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function unsetParameter(string $key) {
    if (isset($this->params[$key])) {
      unset($this->params[$key]);
    }

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function mergeParameters(array $params) {
    $this->params = array_merge($this->params, $params);

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getParameters(): array {
    return $this->params;
  }

  /**
   * {@inheritdoc}
   */
  public function getSourceDomain(): ?string {
    return $this->configFactory
      ->get('imgix.settings')
      ->get('source_domain');
  }

  /**
   * {@inheritdoc}
   */
  public function getExternalCdnDomain(): ?string {
    return $this->configFactory
      ->get('imgix.settings')
      ->get('external_cdn');
  }

  /**
   * {@inheritdoc}
   */
  public function getMappingType(): ?string {
    return $this->configFactory
      ->get('imgix.settings')
      ->get('mapping_type');
  }

  /**
   * {@inheritdoc}
   */
  public function getMappingTypes(): array {
    return [
      static::SOURCE_FOLDER => 'Web Folder',
      static::SOURCE_PROXY => 'Web Proxy',
      static::SOURCE_S3 => 'Amazon S3',
      static::SOURCE_GCS => 'Google Cloud Storage',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getPathPrefix(): ?string {
    return $this->configFactory
      ->get('imgix.settings')
      ->get('path_prefix');
  }

  /**
   * {@inheritdoc}
   */
  public function usesHttps(): bool {
    return (bool) $this->configFactory
      ->get('imgix.settings')
      ->get('https');
  }

  /**
   * {@inheritdoc}
   */
  public function getSecureUrlToken(): ?string {
    return $this->configFactory
      ->get('imgix.settings')
      ->get('secure_url_token');
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $config = $this->configFactory->get('imgix.settings');

    $form['source_domain'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Source domain'),
      '#description' => $this->t('The Imgix domain from which your images are served. Usually, this is a subdomain of imgix.net.'),
      '#default_value' => $config->get('source_domain'),
    ];

    $form['external_cdn'] = [
      '#type' => 'textfield',
      '#title' => $this->t('External CDN'),
      '#description' => $this->t('The domain of an external CDN through which the images should be served, instead of the source domain.'),
      '#default_value' => $config->get('external_cdn'),
    ];

    $form['secure_url_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Secure URL Token'),
      '#description' => $this->t('Signing URLs using a token prevents unauthorized parties from changing the parameters.'),
      '#default_value' => $config->get('secure_url_token'),
      '#states' => [
        'required' => [
          [':input[name="imgix[mapping_type]"]' => ['value' => static::SOURCE_PROXY]],
        ],
      ],
    ];

    $form['mapping_type'] = [
      '#type' => 'radios',
      '#required' => TRUE,
      '#title' => $this->t('Mapping type'),
      '#description' => $this->t('The way Imgix connects to your image storage.'),
      '#options' => $this->getMappingTypes(),
      '#default_value' => $config->get('mapping_type'),
    ];

    $form['mapping_type'][static::SOURCE_S3]['#description'] = $this->t("An Amazon S3 Source connects to an existing Amazon S3 bucket. imgix connects using the credentials you supply, so images don't have to be public.");
    $form['mapping_type'][static::SOURCE_GCS]['#description'] = $this->t("A Google Cloud Storage Source connects to an existing Google Cloud Storage bucket. imgix connects using the credentials you supply, so images don't have to be public.");
    $form['mapping_type'][static::SOURCE_FOLDER]['#description'] = $this->t('A Web Folder Source connects to your existing folder of images that are on a publicly addressable website, usually your website’s existing image folder. <b>Important: <i>Base URL</i> should be set to the website root in the Imgix source settings.</b>');
    $form['mapping_type'][static::SOURCE_PROXY]['#description'] = $this->t('A Web Proxy Source allows you to connect to any image that is addressable through a publicly-available URL. You provide the entire image URL of the master image in the path of the Imgix request.');

    $form['path_prefix'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Path prefix'),
      '#description' => $this->t('A path prefix that should be removed from the image path. When using a GCS or S3 source, this should be the bucket name. This can also be useful in case your images are stored in a subfolder.'),
      '#default_value' => $config->get('path_prefix'),
      '#states' => [
        'visible' => [
          [':input[name="imgix[mapping_type]"]' => ['value' => static::SOURCE_S3]],
          [':input[name="imgix[mapping_type]"]' => ['value' => static::SOURCE_GCS]],
        ],
      ],
    ];

    $form['https'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('HTTPS support'),
      '#default_value' => $config->get('https'),
    ];

    foreach (Element::children($form) as $child) {
      if ($config->get($child) !== $config->getOriginal($child, FALSE)) {
        $form[$child]['#disabled'] = TRUE;
        $form[$child]['#description'] = $this->t('This config cannot be changed because it is overridden.');
      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state): void {
    parent::validateConfigurationForm($form, $form_state);

    $mappingType = $form_state->getValue(['imgix', 'mapping_type']);
    $secureUrlToken = $form_state->getValue(['imgix', 'secure_url_token']);

    if ($mappingType === static::SOURCE_PROXY && empty($secureUrlToken)) {
      $form_state->setErrorByName('imgix][secure_url_token', $this->t('Secure URL Token is required when using Web Proxy mapping type.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $config = $this->configFactory->getEditable('imgix.settings');
    $formImgix = $form_state->getValue('imgix');

    foreach ($formImgix as $key => $value) {
      if (!array_key_exists($key, $formImgix)) {
        continue;
      }

      if ($config->get($key) !== $config->getOriginal($key, FALSE)) {
        // Value has override.
        continue;
      }

      $config->set($key, $value);
    }

    if (!in_array($formImgix['mapping_type'], [ImgixToolkitInterface::SOURCE_GCS, ImgixToolkitInterface::SOURCE_S3])) {
      $config->set('path_prefix', '');
    }

    $config->save();
  }

  /**
   * {@inheritdoc}
   */
  public function useFallbackToolkit($destination = NULL): bool {
    $mappingType = $this->getMappingType();
    $urlScheme = parse_url($destination ?: $this->getSource(), PHP_URL_SCHEME);

    if ($urlScheme === 'private') {
      return TRUE;
    }

    if ($mappingType === static::SOURCE_FOLDER && $urlScheme !== 'public' && $urlScheme !== NULL) {
      return TRUE;
    }

    if ($mappingType === static::SOURCE_S3 && $urlScheme !== 's3') {
      return TRUE;
    }

    if ($mappingType === static::SOURCE_GCS && $urlScheme !== 'gcs') {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public static function isAvailable(): bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   *
   * @see https://docs.imgix.com/apis/rendering/format/fm
   */
  public static function getSupportedExtensions(): array {
    return [
      'gif', 'jp2', 'jpg', 'jpeg', 'json', 'jxr', 'pjpg', 'mp4',
      'png', 'png8', 'png32', 'webm', 'webp', 'blurhash',
    ];
  }

  /**
   * Load the gd toolkit as fallback toolkit.
   *
   * This will be used in case Imgix does not support the current image.
   */
  protected function getFallbackToolkit(): ImageToolkitInterface {
    if (isset($this->fallbackToolkit)) {
      return $this->fallbackToolkit;
    }

    return $this->fallbackToolkit = $this->imageToolkitManager
      ->createInstance('gd')
      ->setSource($this->getSource());
  }

  /**
   * Load the image dimensions into a static cache.
   */
  protected function loadInfo(): void {
    [$this->width, $this->height] = $this->getImageDimensions($this->getSource());
  }

  /**
   * Get the dimensions of a certain image.
   */
  protected function getImageDimensions(string $source): ?array {
    $urlParts = parse_url($source);
    $pathParts = pathinfo($urlParts['path'] ?? '');

    if (isset($pathParts['extension']) && $pathParts['extension'] === 'svg') {
      return $this->getSvgImageDimensions($source);
    }

    $metadata = \Drupal::getContainer()
      ->get(FileMetadataManagerInterface::class)
      ->uri($source);
    if ($metadata === NULL) {
        return NULL;
    }

    $size = $metadata->getMetadata('getimagesize');
    if ($size === NULL) {
        return NULL;
    }

    if (!isset($size[0], $size[1])) {
      return NULL;
    }

    return [$size[0], $size[1]];
  }

  /**
   * Try to determine the dimensions of an SVG image.
   */
  protected function getSvgImageDimensions(string $source): ?array {
    if (!$fileContents = file_get_contents($source)) {
      return NULL;
    }

    $svg = simplexml_load_string($fileContents);
    $attributes = iterator_to_array($svg->attributes());

    if (isset($attributes['width'], $attributes['height'])) {
      return [(int) $attributes['width'], (int) $attributes['height']];
    }

    if (isset($attributes['viewBox'])) {
      $viewBox = $attributes['viewBox'];
    }
    elseif (isset($attributes['viewbox'])) {
      $viewBox = $attributes['viewbox'];
    }

    if (isset($viewBox)) {
      $viewBoxParts = explode(' ', $viewBox);

      if (isset($viewBoxParts[2], $viewBoxParts[3])) {
        return [(int) $viewBoxParts[2], (int) $viewBoxParts[3]];
      }
    }

    return [64, 64];
  }

}
