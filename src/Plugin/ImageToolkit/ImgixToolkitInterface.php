<?php

namespace Drupal\imgix\Plugin\ImageToolkit;

use Drupal\Core\ImageToolkit\ImageToolkitInterface;

/**
 * An image toolkit for Imgix, a real-time image processing service and CDN.
 */
interface ImgixToolkitInterface extends ImageToolkitInterface {

  public const SOURCE_S3 = 's3';
  public const SOURCE_GCS = 'gcs';
  public const SOURCE_FOLDER = 'webfolder';
  public const SOURCE_PROXY = 'webproxy';

  /**
   * Get an Imgix parameter by key.
   */
  public function getParameter(string $key);

  /**
   * Set an Imgix parameter by key.
   */
  public function setParameter(string $key, string $value);

  /**
   * Unset an Imgix parameter by key.
   */
  public function unsetParameter(string $key);

  /**
   * Merge an array of Imgix parameters with the currently stored parameters.
   */
  public function mergeParameters(array $params);

  /**
   * Get all stored Imgix parameters.
   */
  public function getParameters(): array;

  /**
   * Get the Imgix domain from which your images are served.
   */
  public function getSourceDomain(): ?string;

  /**
   * Get the external CDN through which the images should be served.
   */
  public function getExternalCdnDomain(): ?string;

  /**
   * Get the mapping type, the way Imgix connects to your image storage.
   */
  public function getMappingType(): ?string;

  /**
   * Get all available mapping types.
   */
  public function getMappingTypes(): array;

  /**
   * Get the path prefix that should be removed from the image path.
   */
  public function getPathPrefix(): ?string;

  /**
   * Determines whether HTTPS is supported.
   */
  public function usesHttps(): bool;

  /**
   * Get the token that will be used to sign URLs.
   *
   * This prevents unauthorized parties from changing the parameters.
   */
  public function getSecureUrlToken(): ?string;

  /**
   * Determines whether a fallback toolkit should be used.
   *
   * @param string|null $destination
   *   The image destination.
   */
  public function useFallbackToolkit($destination = NULL): bool;

}
