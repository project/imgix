<?php

namespace Drupal\imgix\Plugin\ImageToolkit\Operation;

use Drupal\Core\ImageToolkit\ImageToolkitOperationBase;
use Drupal\imgix\Plugin\ImageToolkit\ImgixToolkit;

/**
 * A base class for Imgix image operations.
 */
abstract class ImgixImageToolkitOperationBase extends ImageToolkitOperationBase {

  /**
   * The correctly typed image toolkit for GD operations.
   *
   * {@inheritdoc}
   */
  protected function getToolkit(): ImgixToolkit {
    return parent::getToolkit();
  }

}
