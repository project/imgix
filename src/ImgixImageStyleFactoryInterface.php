<?php

namespace Drupal\imgix;

use Drupal\image\ImageStyleInterface;

/**
 * Service for converting a set of Imgix parameters to a Drupal image style.
 *
 * To be used while upgrading from older versions of the Imgix module.
 */
interface ImgixImageStyleFactoryInterface {

  /**
   * Convert a set of Imgix parameters to a Drupal image style.
   */
  public function getImageStyleByParameters(array $params): ImageStyleInterface;

}
