<?php

namespace Drupal\imgix\Entity;

use Drupal\image\Entity\ImageStyle;
use Drupal\imgix\Plugin\ImageToolkit\ImgixToolkit;
use Drupal\imgix\Plugin\ImageToolkit\ImgixToolkitInterface;
use Imgix\UrlBuilder;

/**
 * Overrides the image style configuration entity.
 *
 * This class customizes the url generation process to work with Imgix.
 */
class ImgixImageStyle extends ImageStyle {

  /**
   * {@inheritdoc}
   */
  public function buildUri($uri) {
    /** @var \Drupal\Core\Image\ImageInterface $image */
    $image = \Drupal::service('image.factory')->get($uri);
    $toolkit = $image->getToolkit();

    if (!$toolkit instanceof ImgixToolkit || $toolkit->useFallbackToolkit($uri)) {
      return parent::buildUri($uri);
    }

    // We don't need to generate derivatives using this image toolkit,
    // so return the original url.
    return $uri;
  }

  /**
   * {@inheritdoc}
   */
  public function buildUrl($uri, $clean_urls = NULL) {
    /** @var \Drupal\Core\Image\ImageInterface $image */
    $image = \Drupal::service('image.factory')->get($uri);
    $toolkit = $image->getToolkit();

    if (!$toolkit instanceof ImgixToolkit || $toolkit->useFallbackToolkit($uri)) {
      return parent::buildUrl($uri, $clean_urls);
    }

    if (!$sourceDomain = $toolkit->getSourceDomain()) {
      return NULL;
    }

    $url = \Drupal::service('file_url_generator')->generateAbsoluteString($uri);

    if ($toolkit->getMappingType() === ImgixToolkitInterface::SOURCE_PROXY) {
      $path = $url;
    } else {
      $parts = parse_url($url);
      $prefix = $toolkit->getPathPrefix();
      $path = urldecode($parts['path']);

      if ($prefix && substr($path, 0, strlen($prefix)) === $prefix) {
        $path = substr($path, strlen($prefix));
      }
    }

    if (!$path) {
      return NULL;
    }

    $builder = new UrlBuilder($sourceDomain);

    if ($toolkit->usesHttps()) {
      $builder->setUseHttps(TRUE);
    }

    if ($token = $toolkit->getSecureUrlToken()) {
      $builder->setSignKey($token);
    }

    foreach ($this->getEffects() as $effect) {
      $effect->applyEffect($image);
    }

    $url = $builder->createURL($path, $toolkit->getParameters());

    if ($cdnDomain = $toolkit->getExternalCdnDomain()) {
      $url = str_replace($sourceDomain, $cdnDomain, $url);
    }

    return $url;
  }

  /**
   * {@inheritdoc}
   */
  public function flush($path = NULL) {
    // Derivatives are the original image + some query parameters.
    // Don't call parent::flush() because we don't want to delete the original.
  }

}
