<?php

namespace Drupal\imgix;

use Drupal\image\Entity\ImageStyle;
use Drupal\image\ImageStyleInterface;

/**
 * Service for converting a set of Imgix parameters to a Drupal image style.
 */
class ImgixImageStyleFactory implements ImgixImageStyleFactoryInterface {

  /**
   * {@inheritdoc}
   */
  public function getImageStyleByParameters(array $params): ImageStyleInterface {
    /** @var \Drupal\image\Entity\ImageStyle $imageStyle */
    $imageStyle = ImageStyle::create();

    /* @see Convert */
    if (isset($params['fm'])) {
      $imageStyle->addImageEffect([
        'id' => 'image_convert',
        'data' => [
          'extension' => $params['fm'],
        ],
      ]);

      unset($params['fm']);
    }

    /* @see Crop */
    if (isset($params['rect'])) {
      [$x, $y, $width, $height] = explode(',', $params['rect']);

      $imageStyle->addImageEffect([
        'id' => 'image_crop',
        'data' => [
          'x' => (int) $x,
          'y' => (int) $y,
          'width' => (int) $width,
          'height' => (int) $height,
        ],
      ]);

      unset($params['rect']);
    }

    /* @see Desaturate */
    if (isset($params['sat']) && $params['sat'] === -100) {
      $imageStyle->addImageEffect([
        'id' => 'image_desaturate',
      ]);

      unset($params['sat']);
    }

    /* @see Quality */
    if (isset($params['q'])) {
      $imageStyle->addImageEffect([
        'id' => 'imgix_quality',
        'data' => [
          'quality' => (int) $params['q'],
        ],
      ]);

      unset($params['q']);
    }

    /* @see Resize */
    if (isset($params['fit']) && $params['fit'] === 'scale') {
      $imageStyle->addImageEffect([
        'id' => 'image_resize',
        'data' => [
          'width' => (int) $params['w'],
          'height' => (int) $params['h'],
        ],
      ]);

      unset($params['fit'], $params['w'], $params['h']);
    }

    /* @see Rotate */
    if (isset($params['rot'])) {
      $imageStyle->addImageEffect([
        'id' => 'image_rotate',
        'data' => [
          'degrees' => (int) $params['rot'],
          'background' => $params['bg'] ?? NULL,
        ],
      ]);

      unset($params['rot'], $params['bg']);
    }

    /* @see Scale */
    if (isset($params['fit']) && in_array($params['fit'], ['clip', 'max'])) {
      $imageStyle->addImageEffect([
        'id' => 'image_scale',
        'data' => [
          'width' => $params['w'] ?? NULL,
          'height' => $params['h'] ?? NULL,
          'upscale' => $params['fit'] === 'clip',
        ],
      ]);

      unset($params['fit'], $params['w'], $params['h']);
    }

    /* @see ScaleAndCrop */
    if (isset($params['fit']) && in_array($params['fit'], ['crop', 'min'])) {
      $imageStyle->addImageEffect([
        'id' => 'image_scale_and_crop',
        'data' => [
          'width' => $params['w'] ?? NULL,
          'height' => $params['h'] ?? NULL,
          'anchor' => isset($params['crop'])
          ? str_replace(',', '-', $params['crop'])
          : NULL,
          'upscale' => $params['fit'] === 'crop',
        ],
      ]);

      unset($params['fit'], $params['w'], $params['h'], $params['crop']);
    }

    foreach ($params as $key => $value) {
      $imageStyle->addImageEffect([
        'id' => 'imgix_param',
        'data' => [
          'key' => $key,
          'value' => $value,
        ],
      ]);
    }

    // Set weights.
    $effects = $imageStyle->getEffects();
    $weight = -10;

    foreach ($effects as $effect) {
      $effect->setWeight($weight);
      $weight++;
    }

    return $imageStyle;
  }

}
