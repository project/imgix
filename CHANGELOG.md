# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [2.0.0] - 2020-05-23
All development was done in [this fork](https://github.com/wieni/imgix/) and later merged into the Drupal.org project.
For upgrading instructions, please refer to [UPGRADING.md](UPGRADING.md).

### Changed
- Complete rewrite of the module using an image toolkit and image styles.

## [8.x-1.0-beta2] - 2018-09-06
### Fixed
- [Installation error, "The reserved indicator "@" cannot start a plain scalar; you need to quote the scalar at line 5"](https://www.drupal.org/project/imgix/issues/2963502)

## [8.x-1.0-beta1] - 2016-10-10
Initial release for Drupal 8 version coded by @spoit.

## [7.x-1.3] - 2016-08-22
### Fixed
- Fix warnings

## [7.x-1.2] - 2016-08-18
### Changed
- Replace current domain with given Mapping URL
- Reorder fields in the settings page
- Rename some variables

## [7.x-1.1] - 2016-07-23
### Added
- Add uninstall hook to delete all imgix variables

## [7.x-1.0] - 2016-07-23
Initial Drupal 7 release
